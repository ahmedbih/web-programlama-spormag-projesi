﻿using System.Web;
using System.Web.Optimization;

namespace projelayout
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/style.css"));

            bundles.Add(new StyleBundle("~/bundles/custom").Include(
                      "~/Scripts/jquery.nivo.slider.pack.js",
                      "~/Scripts/jquery.meanmenu.min.js",
                      "~/Scripts/wow.min.js",
                      "~/Scripts/jquery-price-slider.js",
                      "~/Scripts/jquery.simpleGallery.min.js",
                      "~/Scripts/jquery.simpleLens.min.js",
                      "~/Scripts/owl.carousel.min.js",
                      "~/Scripts/jquery.scrollUp.min.js",
                      "~/Scripts/jquery.collapse.js",
                      "~/Scripts/plugins.js",
                      "~/Scripts/main.js") );
        }
    }
}
