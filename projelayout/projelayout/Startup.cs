﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(projelayout.Startup))]
namespace projelayout
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
